import cv2
import numpy as np

from PIL import Image


# Функция для вычисления расхода тонера в % RGB файла
def calc_toner_usage_rgb(img_path):
    img = cv2.imread(img_path)

    blue, green, red = cv2.split(img)

    total_pixels = img.shape[0] * img.shape[1]
    total_red = red.sum()
    total_green = green.sum()
    total_blue = blue.sum()

    toner_cyan = 100 - (total_red / (255 * total_pixels)) * 100
    toner_magenta = 100 - (total_green / (255 * total_pixels)) * 100
    toner_yellow = 100 - (total_blue / (255 * total_pixels)) * 100
    if (np.max(img) != 255):
        toner_black = (255 - np.max(img)) / 2.55
    else:
        toner_black = np.sum(img == 255) / total_pixels * 100

    return toner_cyan, toner_magenta, toner_yellow, toner_black


# Функция для вычисления расхода тонера в % CMYK файла
def calc_toner_usage_cmyk(img_path):
    cmyk_img = Image.open(img_path)#.convert("CMYK")

    cyan = cmyk_img.getchannel("C").getdata()
    magenta = cmyk_img.getchannel("M").getdata()
    yellow = cmyk_img.getchannel("Y").getdata()
    black = cmyk_img.getchannel("K").getdata()

    total_pixels = cmyk_img.size[0] * cmyk_img.size[1]
    total_cyan = sum(cyan)
    total_magenta = sum(magenta)
    total_yellow = sum(yellow)
    total_black = sum(black)

    toner_cyan = total_cyan / (255 * total_pixels) * 100
    toner_magenta = total_magenta / (255 * total_pixels) * 100
    toner_yellow = total_yellow / (255 * total_pixels) * 100
    toner_black = total_black / (255 * total_pixels) * 100

    return toner_cyan, toner_magenta, toner_yellow, toner_black


# Функция для вычисления расхода тонера в % L файла
def calc_toner_usage_gray(img_path):
    img = cv2.imread(img_path)

    blue = cv2.split(img)[0]

    total_blue = blue.sum()
    toner_blue = total_blue / (255 * img.shape[0] * img.shape[1]) * 100

    return toner_blue


def is_gray(img_path):
    img = cv2.imread(img_path)
    for x in range(img.shape[0]):
        for y in range(img.shape[1]):
            (b, g, r) = img[x, y]
            if not (b == g == r):
                return False

    return True


def is_bw(img_path):
    img = cv2.imread(img_path)
    for x in range(img.shape[0]):
        for y in range(img.shape[1]):
            (b, g, r) = img[x, y]
            if not (b == g == r == (255 or 0)):
                return False

    return True
