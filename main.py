import polygraphy as pol
from PIL import Image


def calc_toner_usage(img_path):
    img = Image.open(img_path)
    match img.mode:
        case 'CMYK':
            toner_cyan, toner_magenta, toner_yellow, toner_black =\
                pol.calc_toner_usage_cmyk(img_path)
            print(f"Расход тонера голубого цвета: {toner_cyan:.2f}%")
            print(f"Расход тонера пурпурного цвета: {toner_magenta:.2f}%")
            print(f"Расход тонера жёлтого цвета: {toner_yellow:.2f}%")
            print(f"Расход тонера чёрного цвета: {toner_black:.2f}%")

        case 'RGB':
            toners = pol.calc_toner_usage_rgb(img_path)
            print(f"Расход тонера голубого цвета: {toners[0]:.2f}%")
            print(f"Расход тонера пурпурного цвета: {toners[1]:.2f}%")
            print(f"Расход тонера жёлтого цвета: {toners[2]:.2f}%")
            print(f"Расход тонера чёрного цвета: {toners[3]:.2f}%")

        case 'L':
            toners = pol.calc_toner_usage_gray(img_path)
            print(f"Расход тонера голубого цвета: {toners:.2f}%")
            print(f"Расход тонера пурпурного цвета: {toners:.2f}%")
            print(f"Расход тонера жёлтого цвета: {toners:.2f}%")
            print(f"Расход тонера чёрного цвета: {0:.2f}%")

        case _:
            print("Sorry, there is no function for this type of file.")
            print("You can send an email to me to report and code will be\
            fixed:)")


if __name__ == "__main__":
    img_path = input("Input a file name/path:\n")
    calc_toner_usage(img_path)
